import { TOKEN_NAME } from '@constants/token.constant';
import jwtDecode from 'jwt-decode';

export const setToken = (token) => {
  localStorage.setItem(TOKEN_NAME, token);
};

export const getToken = () => localStorage.getItem(TOKEN_NAME) || '';

export const getPayloadToken = (token) => jwtDecode(token);

export const hasExpiredToken = () => {
  const token = getToken();
  if (!token) return true;

  const payload = jwtDecode(token);
  const expireDate = payload.exp * 1000;
  const currentDate = new Date().getTime();

  return currentDate > expireDate;
};

export const removeToken = () => localStorage.removeItem(TOKEN_NAME);
