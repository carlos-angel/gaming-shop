export const codeMessageError = {
  'Auth.form.error.email.taken': 'Email y/o password inválidos',
  'Auth.form.error.invalid': 'Email y/o password inválidos',
  'default.error': 'Ops! Algo paso. Inténtelo más tarde',
};

export const getMessageErrorStrapi = (errorStrapi) => {
  const { id } = errorStrapi?.message[0]?.messages[0] ?? { id: 'default.error' };
  return codeMessageError[id] ?? 'ops! Algo paso. Inténtelo más tarde';
};
