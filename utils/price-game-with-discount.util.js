export function priceGameWithDiscount(price, discount = 0) {
  return (price - Math.floor((price * discount) / 100)).toFixed(2);
}
