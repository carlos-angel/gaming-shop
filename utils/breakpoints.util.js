export const breakpoints = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
};

export function getColumnsResponsive(width) {
  if (width >= breakpoints.xl) {
    return 5;
  }

  if (width >= breakpoints.lg) {
    return 4;
  }

  if (width >= breakpoints.md) {
    return 3;
  }

  if (width >= breakpoints.sm) {
    return 2;
  }

  return 1;
}
