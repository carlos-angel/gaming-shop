import { ToastContainer } from 'react-toastify';
import { ProviderAuth } from '@context/Auth.context';
import { ProviderCart } from '@context/cart';
import 'semantic-ui-css/semantic.min.css';
import '@scss/globals.scss';
import 'react-toastify/dist/ReactToastify.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

function MyApp({ Component, pageProps }) {
  return (
    <ProviderCart>
      <ProviderAuth>
        <Component {...pageProps} />
        <ToastContainer
          position='top-right'
          autoClose={50000}
          hideProgressBar
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnFocusLoss={false}
          draggable
          pauseOnHover
        />
      </ProviderAuth>
    </ProviderCart>
  );
}

export default MyApp;
