import { useRouter } from 'next/router';
import { Button, Loader, Tab } from 'semantic-ui-react';
import { Layout } from '@components/Layout';
import { useData } from '@hooks/useData.hook';
import { getGame } from '@services/api/game.api';
import Game from '@components/Game';
import { PaneInformation } from '@components/Game/Panes';
import ReactPlayer from 'react-player/lazy';
import Screenshots from '@components/Game/Screenshots';
import moment from 'moment';
import 'moment/locale/es';
import { useCart } from '@hooks/useCart.hook';

export default function GamePage() {
  const {
    query: { game = '' },
  } = useRouter();
  const { addToCart } = useCart();

  const { data, loading } = useData({
    func: getGame,
    params: { url: game },
    paramsEffect: [game],
  });

  const panes = [
    {
      menuItem: 'Información',
      render: () => (
        <PaneInformation>
          <ReactPlayer className='pane-information__video' url={data?.video_url} controls={true} />
          <Screenshots screenshots={data?.screenshots} title={data?.title} />
          <div className='pane-information__content'>
            <div dangerouslySetInnerHTML={{ __html: data?.summary }} />
            <div className='pane-information__content-date'>
              <h4>Fecha de lanzamiento:</h4>
              <p>{moment(data?.release_date).format('LL')}</p>
            </div>
          </div>
        </PaneInformation>
      ),
    },
  ];

  return (
    <Layout title='game' className='game' description={data.summary}>
      {loading && <Loader active inline='centered' />}
      {data && !loading && (
        <Game>
          <Game.Poster src={data?.poster?.url} alt={data?.title} />
          <Game.Information>
            <Game.Title title={data.title} gameId={data?.id} />
            <Game.Delivery text='Plazo de entrega de 24/48hr.' />
            <Game.Summary text={data.summary} />

            <Game.Buy price={data.price} discount={data?.discount || 0}>
              <Button
                className='header-game__buy-btn'
                onClick={() => addToCart({ product: data, quantity: 1 })}
              >
                Comprar
              </Button>
            </Game.Buy>
          </Game.Information>
        </Game>
      )}

      {data && !loading && <Tab className='game__tabs' panes={panes} />}
    </Layout>
  );
}
