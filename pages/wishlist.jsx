import { Loader } from 'semantic-ui-react';
import map from 'lodash/map';
import size from 'lodash/size';
import { Layout } from '@components/Layout';
import { useAuth } from '@hooks/useAuth';
import { useData } from '@hooks/useData.hook';
import { getFavoritesGames } from '@services/api/favorite.api';
import Games from '@components/Games/Games';

export default function Wishlist() {
  const { user } = useAuth();
  const userId = user?.id;
  const { data, loading } = useData({
    func: getFavoritesGames,
    params: { userId },
  });

  return (
    <Layout title='wishlist' className='wishlist'>
      <div className='wishlist__block'>
        <div className='title'>Lista de favoritos</div>
        <div className='data'>
          {loading && <Loader active inline='centered' />}
          {size(data) === 0 && !loading && <p className='data__not-found'>No hay favoritos</p>}
          {size(data) > 0 && !loading && (
            <Games>
              {map(data, (game) => (
                <Games.Item key={game.id} {...game} />
              ))}
            </Games>
          )}
        </div>
      </div>
    </Layout>
  );
}
