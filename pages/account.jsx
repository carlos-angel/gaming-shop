import { ChangeNameForm } from '@components/Auth/ChangeNameForm';
import { ChangeEmailForm } from '@components/Auth/ChangeEmailForm';
import { ChangePasswordForm } from '@components/Auth/ChangePasswordForm';
import { Layout } from '@components/Layout';
import { useAuth } from '@hooks/useAuth';
import { useModal } from '@hooks/useModal';
import { BasicModal } from '@components/Modal/BasicModal';
import Addresses from '@components/Account/Addresses';

export default function Account() {
  const { user } = useAuth();
  const { isOpen, title, content, openModal, closeModal } = useModal();
  if (!user) return null;

  return (
    <Layout title='account' className='account'>
      <section className='account__configuration'>
        <div className='title'>Configuración</div>
        <div className='data'>
          <ChangeNameForm name={user.name} lastName={user.lastName} />
          <ChangeEmailForm currentEmail={user.email} />
          <ChangePasswordForm />
        </div>
      </section>

      <Addresses userId={user.id} openModal={openModal} closeModal={closeModal} />

      <BasicModal show={isOpen} setShow={closeModal} title={title}>
        {content}
      </BasicModal>
    </Layout>
  );
}
