import { Layout } from '@components/Layout';
import { Loader } from 'semantic-ui-react';
import { useData } from '@hooks/useData.hook';
import { getLastGames } from '@services/api/game.api';
import map from 'lodash/map';
import size from 'lodash/size';
import Games from '@components/Games/Games';

export default function Home() {
  const { data, loading } = useData({ func: getLastGames, params: { limit: 10 } });

  return (
    <Layout title='home' className='home'>
      {loading && <Loader active inline='centered' />}
      {size(data) === 0 && !loading && <p>No games found</p>}
      {size(data) > 0 && (
        <Games>
          {map(data, (game) => (
            <Games.Item key={game.id} {...game} />
          ))}
        </Games>
      )}
    </Layout>
  );
}
