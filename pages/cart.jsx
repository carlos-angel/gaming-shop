import { Table, Image, Icon } from 'semantic-ui-react';
import map from 'lodash/map';
import size from 'lodash/size';
import { Layout } from '@components/Layout';
import { useCart } from '@hooks/useCart.hook';
import AddressShipping from '@components/Cart/AddressShipping';
import Payment from '@components/Cart/Payment';
import { useAuth } from '@hooks/useAuth';

export default function Cart() {
  const { user } = useAuth();
  const { cart, removeFromCart, address } = useCart();
  const isUser = user?.id;
  const isCartEmpty = size(cart) === 0;
  const isAddressSelected = address?.id;

  let total = 0;
  return (
    <Layout title='cart' className='cart'>
      {isCartEmpty && <p className='cart__not-found'>No hay juegos en el carrito</p>}
      {!isCartEmpty && (
        <div className='cart__summary'>
          <div className='cart__summary-title'>Resumen del carrito</div>

          <div className='cart__summary-data'>
            <Table celled structured>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Juego</Table.HeaderCell>
                  <Table.HeaderCell>Plataforma</Table.HeaderCell>
                  <Table.HeaderCell>Cantidad</Table.HeaderCell>
                  <Table.HeaderCell>Precio</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {map(cart, ({ product, quantity }) => {
                  total += product.price * quantity;
                  return (
                    <Table.Row key={product.id} className='cart__summary-product'>
                      <Table.Cell>
                        <Icon name='close' link onClick={() => removeFromCart({ product })} />
                        <Image src={product.poster.url} alt={product.title} />
                        {product.title}
                      </Table.Cell>
                      <Table.Cell>{product.platform.title}</Table.Cell>
                      <Table.Cell>{quantity}</Table.Cell>
                      <Table.Cell>{`$${product.price}`}</Table.Cell>
                    </Table.Row>
                  );
                })}
                <Table.Row className='cart__resumen'>
                  <Table.Cell className='cart__resumen-clear' />
                  <Table.Cell colSpan='2'>Total:</Table.Cell>
                  <Table.Cell className='cart__resumen-total'>{`$${total.toFixed(2)}`}</Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </div>
        </div>
      )}

      {isUser && <AddressShipping userId={user.id} />}

      {isAddressSelected ? (
        <Payment products={cart} address={address} />
      ) : (
        <span>Seleccione una dirección de envió para proceder con el pago</span>
      )}
    </Layout>
  );
}
