import { useRouter } from 'next/router';
import { Loader } from 'semantic-ui-react';
import { Layout } from '@components/Layout';
import { useData } from '@hooks/useData.hook';
import { getGamesPlatform, getTotalGamesPlatform } from '@services/api/game.api';
import map from 'lodash/map';
import size from 'lodash/size';
import Games from '@components/Games/Games';
import Pagination from '@components/Common/Pagination';

const LIMIT_PAGE = 5;
export default function Platforms() {
  const {
    query: { platform, page = 1 },
  } = useRouter();
  const start = (page - 1) * LIMIT_PAGE;

  const { data, loading, setReloadData } = useData({
    func: getGamesPlatform,
    params: { platform, limit: LIMIT_PAGE, start },
    paramsEffect: [platform, start],
  });

  const totalGames = useData({
    func: getTotalGamesPlatform,
    params: { platform },
    paramsEffect: [platform],
  });

  return (
    <Layout title='platform' className='platform'>
      {loading && <Loader active inline='centered' />}
      {size(data) === 0 && !loading && <p>No games found</p>}
      {size(data) > 0 && !loading && (
        <Games>
          {map(data, (game) => (
            <Games.Item key={game.id} {...game} />
          ))}
        </Games>
      )}

      {!totalGames.loading && totalGames.data && (
        <Pagination total={totalGames.data} page={page} limit={LIMIT_PAGE} />
      )}
    </Layout>
  );
}
