import { useState, useEffect } from 'react';
import { Layout } from '@components/Layout';
import { useData } from '@hooks/useData.hook';
import { useRouter } from 'next/router';
import map from 'lodash/map';
import size from 'lodash/size';
import { searchGames } from '@services/api/game.api';
import { Loader } from 'semantic-ui-react';
import Games from '@components/Games/Games';

export default function Search() {
  const { query } = useRouter();
  const { q } = query;
  const { data, loading } = useData({
    func: searchGames,
    params: { q },
    paramsEffect: [q],
  });
  useEffect(() => {
    document.getElementById('search-game').focus();
  }, []);

  return (
    <Layout title={`Buscando: ${q}`} className='search'>
      {loading && <Loader active inline='centered' />}
      {size(data) === 0 && !loading && <p>No games found</p>}
      {size(data) > 0 && !loading && (
        <Games>
          {map(data, (game) => (
            <Games.Item key={game.id} {...game} />
          ))}
        </Games>
      )}
    </Layout>
  );
}
