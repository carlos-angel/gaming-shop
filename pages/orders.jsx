import { useEffect, useState } from 'react';
import { Layout } from '@components/Layout';
import { getOrderByUserId } from '@services/api/orders.api';
import { useAuth } from '@hooks/useAuth';
import { Orders } from '@components/Orders';

export default function OrdersPage() {
  const [orders, setOrders] = useState([]);
  const { user } = useAuth();

  useEffect(() => {
    (async () => {
      if (user?._id) {
        const { data } = await getOrderByUserId(user._id);
        setOrders(data || []);
      }
    })();
  }, [user]);

  return (
    <Layout title='home' className='orders'>
      <div className='orders__block'>
        <div className='title'>Mis pedidos</div>
        <div className='data'>
          <p>Lista de pedidos</p>
          <Orders orders={orders} />
        </div>
      </div>
    </Layout>
  );
}
