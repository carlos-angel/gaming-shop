import axios from 'axios';
import endpoints from '@services/api';
import { getMessageErrorStrapi } from '@utils/getMessageErrorStrapi.util';

export const isFavoriteGame = async ({ userId, gameId, config }) => {
  try {
    const { data } = await axios.get(endpoints.favorite.isFavoriteGame({ gameId, userId }), config);
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const addFavoriteGame = async ({ userId, gameId, config }) => {
  try {
    const { data } = await axios.post(
      endpoints.favorite.add,
      { user: userId, game: gameId },
      config,
    );
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const deleteFavoriteGame = async ({ id, config }) => {
  try {
    const { data } = await axios.delete(endpoints.favorite.delete({ id }), config);
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const getFavoritesGames = async ({ userId, config }) => {
  try {
    const { data } = await axios.get(endpoints.favorite.getAllByUser({ userId }), config);
    const favorites = data.length > 0 ? data.map((favorite) => favorite.game) : [];
    return { error: false, message: '', data: favorites };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};
