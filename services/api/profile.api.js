import axios from 'axios';
import endpoints from '@services/api';
import { getMessageErrorStrapi } from '@utils/getMessageErrorStrapi.util';

export const getProfile = async () => {
  try {
    const { data } = await axios.get(endpoints.profile.getMe);
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const updateProfile = async ({ id, body }) => {
  try {
    const { data } = await axios.put(endpoints.profile.update({ id }), body);
    return { error: false, message: 'datos actualizados', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};
