import { BASE_API } from 'constants/api.constant';

const endpoints = {
  auth: {
    register: `${BASE_API}/auth/local/register`,
    login: `${BASE_API}/auth/local`,
    forgotPassword: `${BASE_API}/auth/forgot-password`,
  },
  profile: {
    getMe: `${BASE_API}/users/me`,
    update: ({ id }) => `${BASE_API}/users/${id}`,
  },
  address: {
    create: `${BASE_API}/addresses`,
    getAllByUser: ({ id }) => `${BASE_API}/addresses?user=${id}`,
    delete: ({ id }) => `${BASE_API}/addresses/${id}`,
    update: ({ id }) => `${BASE_API}/addresses/${id}`,
  },
  platforms: {
    getAll: `${BASE_API}/platforms?_sort=position:asc`,
  },
  games: {
    getLastGames: ({ limit = 10 }) => `${BASE_API}/games?_limit=${limit}&_sort=createdAt:desc`,
    getGamesPlatform: ({ platform, limit = 10, start }) =>
      `${BASE_API}/games?platform.url=${platform}&_limit=${limit}&_sort=createdAt:desc&_start=${start}`,
    getTotalGamesPlatform: ({ platform }) => `${BASE_API}/games/count?platform.url=${platform}`,
    getGameByUrl: ({ url }) => `${BASE_API}/games?url=${url}`,
    search: ({ q }) => `${BASE_API}/games?_q=${q}`,
  },
  favorite: {
    isFavoriteGame: ({ gameId, userId }) => `${BASE_API}/favorites?game=${gameId}&user=${userId}`,
    add: `${BASE_API}/favorites`,
    delete: ({ id }) => `${BASE_API}/favorites/${id}`,
    getAllByUser: ({ userId }) => `${BASE_API}/favorites?user=${userId}`,
  },
  orders: {
    create: `${BASE_API}/orders`,
    byUser: ({ id }) => `${BASE_API}/orders?_sort=createdAt:desc&user=${id}`,
  },
};

export default endpoints;
