import axios from 'axios';
import endpoints from '@services/api';
import { getMessageErrorStrapi } from '@utils/getMessageErrorStrapi.util';

export const register = async (body) => {
  const config = {
    headers: {
      'Content-type': 'application/json',
    },
  };

  try {
    const { data } = await axios.post(endpoints.auth.register, body, config);
    return { error: false, message: 'usuario registrado correctamente', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const login = async ({ identifier, password }) => {
  const config = {
    headers: {
      'Content-type': 'application/json',
    },
  };

  try {
    const { data } = await axios.post(endpoints.auth.login, { identifier, password }, config);
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const forgotPassword = async ({ email }) => {
  const config = {
    headers: {
      'Content-type': 'application/json',
    },
  };

  try {
    const { data } = await axios.post(endpoints.auth.forgotPassword, { email }, config);
    return {
      error: false,
      message: 'Se ha enviado un correo electrónico para que recuperes tu cuenta',
      data,
    };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};
