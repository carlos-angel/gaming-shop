import axios from 'axios';
import endpoints from '@services/api';
import { getMessageErrorStrapi } from '@utils/getMessageErrorStrapi.util';

export const getLastGames = async ({ limit }) => {
  try {
    const { data } = await axios.get(endpoints.games.getLastGames({ limit }));
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const getGamesPlatform = async ({ platform, start, limit }) => {
  try {
    const { data } = await axios.get(endpoints.games.getGamesPlatform({ platform, start, limit }));
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const getTotalGamesPlatform = async ({ platform }) => {
  try {
    const { data } = await axios.get(endpoints.games.getTotalGamesPlatform({ platform }));
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const getGame = async ({ url }) => {
  try {
    const { data } = await axios.get(endpoints.games.getGameByUrl({ url }));

    return {
      error: false,
      message: '',
      data: data[0] || null,
    };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const searchGames = async ({ q, config }) => {
  try {
    const { data } =
      q.length > 0 ? await axios.get(endpoints.games.search({ q }), config) : { data: [] };
    return { error: false, message: '', data };
  } catch (error) {
    const message = error?.response?.data
      ? getMessageErrorStrapi(errorStrapi)
      : error?.request
      ? error.request
      : error.message;
    return { error: true, message, data: null };
  }
};
