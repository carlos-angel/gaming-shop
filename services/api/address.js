import axios from 'axios';
import endpoints from '@services/api';
import { getMessageErrorStrapi } from '@utils/getMessageErrorStrapi.util';

export const createAddress = async (values) => {
  try {
    const { data } = await axios.post(endpoints.address.create, values);
    return { error: false, message: 'dirección guardada correctamente', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const getAddressesByUser = async ({ userId }) => {
  try {
    const { data } = await axios.get(endpoints.address.getAllByUser({ id: userId }));
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const deleteAddress = async ({ addressId }) => {
  try {
    const { data } = await axios.delete(endpoints.address.delete({ id: addressId }));
    return { error: false, message: 'dirección eliminada correctamente', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};

export const updateAddress = async ({ id, values }) => {
  try {
    const { data } = await axios.put(endpoints.address.update({ id }), values);
    return { error: false, message: 'dirección actualizada correctamente', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};
