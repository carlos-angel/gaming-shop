import axios from 'axios';
import endpoints from '@services/api';
import { getMessageErrorStrapi } from '@utils/getMessageErrorStrapi.util';

export const getPlatforms = async () => {
  try {
    const { data } = await axios.get(endpoints.platforms.getAll);
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};
