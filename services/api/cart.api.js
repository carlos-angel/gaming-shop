import axios from 'axios';
import endpoints from '@services/api';

export const paymentCart = async ({ token, products, idUser, address }) => {
  try {
    const addressShipping = address;
    delete addressShipping.user;
    delete addressShipping.createAt;

    const productsCart = products.map(({ product }) => ({ ...product }));

    const body = {
      token,
      products: productsCart,
      idUser,
      addressShipping,
    };

    const { data } = await axios.post(endpoints.orders.create, body);
    return { error: false, message: 'order procesada correctamente', data };
  } catch (error) {
    return { error: true, message: 'Error al procesar el pago', data: null };
  }
};
