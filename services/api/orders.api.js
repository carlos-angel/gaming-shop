import axios from 'axios';
import endpoints from '@services/api';
import { getMessageErrorStrapi } from '@utils/getMessageErrorStrapi.util';

export const getOrderByUserId = async (id) => {
  try {
    const { data } = await axios.get(endpoints.orders.byUser({ id }));
    return { error: false, message: '', data };
  } catch (error) {
    const errorStrapi = error.response.data;
    const message = getMessageErrorStrapi(errorStrapi);
    return { error: true, message, data: null };
  }
};
