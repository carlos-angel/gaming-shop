export const INITIAL_STATE = {
  cart: [],
  address: {},
  loading: false,
  error: null,
};

const types = {
  ADD_TO_CART: 'ADD_TO_CART',
  ADD_ADDRESS: 'ADD_ADDRESS',
  REMOVE_FROM_CART: 'REMOVE_FROM_CART',
  UPDATE_CART_ITEM: 'UPDATE_CART_ITEM',
  CLEAR_CART: 'CLEAR_CART',
  LOADING: 'LOADING',
  ERROR: 'ERROR',
  LOAD_CART: 'LOAD CART',
};

export const cartReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_TO_CART:
      return {
        ...state,
        cart: [...state.cart, action.payload],
      };
    case types.LOAD_CART:
      return {
        ...state,
        cart: [...action.payload],
      };
      break;
    case types.REMOVE_FROM_CART:
      return {
        ...state,
        cart: state.cart.filter((item) => item.product.id !== action.payload.product.id),
      };
    case types.UPDATE_CART_ITEM:
      return {
        ...state,
        cart: state.cart.map((item) => {
          if (item.product.id === action.payload.product.id) {
            return {
              ...item,
              quantity: action.payload.quantity,
            };
          }
          return item;
        }),
      };
    case types.CLEAR_CART:
      return {
        cart: [],
        address: {},
        loading: false,
        error: null,
      };
    case types.ADD_ADDRESS:
      return {
        ...state,
        address: action.payload,
      };
    case types.LOADING:
      return {
        ...state,
        loading: true,
      };
    case types.ERROR:
      return {
        ...state,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};

export const actions = {
  addToCart: ({ product, quantity }) => ({
    type: types.ADD_TO_CART,
    payload: { product, quantity },
  }),
  loadCart: (products) => ({
    type: types.LOAD_CART,
    payload: products,
  }),
  removeFromCart: ({ product }) => ({
    type: types.REMOVE_FROM_CART,
    payload: { product },
  }),
  updateCartItem: ({ product, quantity }) => ({
    type: types.UPDATE_CART_ITEM,
    payload: { product, quantity },
  }),
  clearCart: () => ({
    type: types.CLEAR_CART,
  }),
  addAddress: (address) => ({
    type: types.ADD_ADDRESS,
    payload: address,
  }),
  loading: () => ({ type: types.LOADING }),
  error: () => ({ type: types.ERROR }),
};
