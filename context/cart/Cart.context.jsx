import { createContext } from 'react';
import { useCartProvider } from './useCartProvider.hook';

export const CartContext = createContext({
  cart: [],
  quantity: 0,
  address: {},
  addAddress: (address) => {},
  addToCart: ({ product, quantity }) => {},
  removeFromCart: ({ product }) => {},
  updateCart: ({ product, quantity }) => {},
  clearCart: () => {},
});

export function ProviderCart({ children }) {
  const state = useCartProvider();
  return <CartContext.Provider value={state}>{children}</CartContext.Provider>;
}
