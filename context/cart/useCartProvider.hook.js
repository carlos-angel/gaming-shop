import { useEffect, useMemo, useReducer } from 'react';
import { INITIAL_STATE, cartReducer, actions } from './cart.reducer';
import { CART } from '@constants/cart.constant';
import { toast } from 'react-toastify';
import { includes, remove, size } from 'lodash';
import { getGame } from '@services/api/game.api';

export function useCartProvider() {
  const [state, dispatch] = useReducer(cartReducer, INITIAL_STATE);

  useEffect(() => {
    (async () => {
      const productsTemp = [];
      const products = getProductsCart();
      console.log(products);

      if (products) {
        for await (const product of products) {
          const resp = await getGame({ url: product });
          productsTemp.push({ product: resp.data, quantity: 1 });
        }
        dispatch(actions.loadCart(productsTemp));
      }
    })();
  }, []);

  const quantity = useMemo(() => {
    return state.cart.reduce((acc, item) => {
      return acc + item.quantity;
    }, 0);
  }, [state.cart]);

  const addToCart = ({ product, quantity }) => {
    const productExists = state.cart.find((item) => item.product.id === product.id);

    if (productExists) {
      // cuando se necesite registrar que la cantidad de un producto sea mayor a uno
      //updateCartItem({ product, quantity: productExists.quantity + quantity });
      toast.warning('el producto ya esta en el carrito');
    } else {
      dispatch(actions.addToCart({ product, quantity }));
      addProductCart(product.url);
    }
  };
  const removeFromCart = ({ product }) => {
    dispatch(actions.removeFromCart({ product }));
    removeProductCart(product.url);
  };

  const updateCartItem = ({ product, quantity }) =>
    dispatch(actions.updateCartItem({ product, quantity }));

  const clearCart = () => {
    dispatch(actions.clearCart());
    removeAllProductsCart();
  };

  const addAddress = (address) => dispatch(actions.addAddress(address));

  const data = useMemo(
    () => ({
      ...state,
      quantity,
      addToCart,
      removeFromCart,
      updateCartItem,
      clearCart,
      addAddress,
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [state.cart, state.address, quantity],
  );

  return { ...data };
}

export function getProductsCart() {
  const cart = localStorage.getItem(CART);

  if (!cart) {
    return null;
  } else {
    const products = cart.split(',');
    return products;
  }
}

function addProductCart(product) {
  const cart = getProductsCart();

  if (!cart) {
    localStorage.setItem(CART, product);
    toast.success('Producto añadido al carrito');
  } else {
    const productFound = includes(cart, product);
    if (productFound) {
      toast.warning('Este producto ya esta en el carrito');
    } else {
      cart.push(product);
      localStorage.setItem(CART, cart);
      toast.success('Producto añadido correctamente');
    }
  }
}

function removeProductCart(product) {
  const cart = getProductsCart();

  remove(cart, (item) => {
    return item === product;
  });

  if (size(cart) > 0) {
    localStorage.setItem(CART, cart);
  } else {
    localStorage.removeItem(CART);
  }
}

export function removeAllProductsCart() {
  localStorage.removeItem(CART);
}
