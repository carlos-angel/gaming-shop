import { useEffect, useState, createContext } from 'react';
import { getToken, removeToken, setToken, hasExpiredToken } from '@utils/token.util';
import axios from 'axios';
import { getProfile } from '@services/api/profile.api';

const INITIAL_STATE = {
  user: {},
  isAuth: false,
  signIn: (token) => {},
  signOut: () => {},
};

export const AuthContext = createContext(INITIAL_STATE);

export const ProviderAuth = ({ children }) => {
  const auth = useProviderAuth();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};

const useProviderAuth = () => {
  const [user, setUser] = useState(null);
  const [reloadUser, setReloadUser] = useState(false);

  useEffect(() => {
    (async () => {
      if (!hasExpiredToken()) {
        const token = getToken();
        await signIn(token);
      } else {
        await signOut();
      }
    })();
  }, []);

  useEffect(() => {
    if (reloadUser) {
      (async () => {
        const { data } = await getProfile();
        setUser(data);
        setReloadUser(false);
      })();
    }
  }, [reloadUser]);

  const signIn = async (token) => {
    setToken(token);
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    const { data } = await getProfile();
    setUser(data);
  };
  const signOut = () => {
    delete axios.defaults.headers.common['Authorization'];
    removeToken();
    setUser(null);
  };
  const isAuth = !!user;

  return { user, isAuth, signIn, signOut, setReloadUser };
};
