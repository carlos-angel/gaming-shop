export const INITIAL_STATE = {
  data: null,
  error: null,
  loading: false,
};

const types = {
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

export const dataReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case types.SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        error: false,
      };
    case types.ERROR:
      return {
        ...state,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};

export const actions = {
  loading: () => ({ type: types.LOADING }),
  success: (payload) => ({ type: types.SUCCESS, payload }),
  error: () => ({ type: types.ERROR }),
};
