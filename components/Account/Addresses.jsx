import { deleteAddress, getAddressesByUser } from '@services/api/address';
import React, { useReducer } from 'react';
import { Grid, Button, Icon } from 'semantic-ui-react';
import map from 'lodash/map';
import { useData } from '@hooks/useData.hook';
import { AddressFrom } from './AddressFrom';
import Loading from '@components/Common/Loading';
import { INITIAL_STATE, dataReducer, actions } from 'reducers/data.reducer';
import { toast } from 'react-toastify';

export default function Addresses({ userId, closeModal, openModal }) {
  const {
    data: addresses,
    loading,
    setReloadData,
  } = useData({
    func: getAddressesByUser,
    params: { userId },
  });

  const onPlusAddress = () =>
    openModal({
      title: 'Nueva dirección',
      content: <AddressFrom closeModal={closeModal} setReloadData={setReloadData} />,
    });

  const onEditAddress = (address) =>
    openModal({
      title: `Editar dirección ${address.title}`,
      content: (
        <AddressFrom
          closeModal={closeModal}
          setReloadData={setReloadData}
          initialState={address}
          isEdit
        />
      ),
    });

  return (
    <section className='account__addresses'>
      <div className='account__addresses'>
        <div className='title'>
          Direcciones <Icon name='plus' link onClick={onPlusAddress} />
        </div>
        <div className='data'>
          <div className='list-address'>
            {loading && <Loading content='Cargando' />}
            {!loading && (
              <Grid>
                {map(addresses, (address) => (
                  <Grid.Column key={address.id} mobile={16} tablet={8} computer={4}>
                    <Address
                      {...address}
                      setReloadData={setReloadData}
                      onEditAddress={onEditAddress}
                    />
                  </Grid.Column>
                ))}
              </Grid>
            )}
          </div>
        </div>
      </div>
    </section>
  );
}

function Address(props) {
  const [addressDelete, dispatchAddressDelete] = useReducer(dataReducer, INITIAL_STATE);
  const { id, title, name, address, city, postalCode, state, phone, setReloadData, onEditAddress } =
    props;
  const onDelete = async (addressId) => {
    dispatchAddressDelete(actions.loading());
    try {
      const { error, message, data } = await deleteAddress({ addressId });
      if (error) {
        dispatchAddressDelete(actions.error());
        toast.error(message);
      } else {
        dispatchAddressDelete(actions.success(data));
        toast.success(message);
        setReloadData();
      }
    } catch (error) {
      dispatchAddressDelete(actions.error());
      toast.error(error.message);
    }
  };
  return (
    <div className='address'>
      <p>{title}</p>
      <p>{name}</p>
      <p>{address}</p>
      <p>{`${state}, ${city} ${postalCode}`}</p>
      <p>{phone}</p>

      <div className='actions'>
        <Button
          primary
          onClick={() =>
            onEditAddress({ id, title, name, address, city, postalCode, state, phone })
          }
        >
          Editar
        </Button>
        <Button
          disabled={addressDelete.loading}
          loading={addressDelete.loading}
          onClick={async () => await onDelete(id)}
        >
          Eliminar
        </Button>
      </div>
    </div>
  );
}
