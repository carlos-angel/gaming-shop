import { Button } from 'semantic-ui-react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { FormGroup } from 'semantic-ui-react';
import { Input } from '@components/Form';
import { createAddress, updateAddress } from '@services/api/address';
import { useAuth } from '@hooks/useAuth';

export const AddressFrom = ({ closeModal, setReloadData, initialState, isEdit = false }) => {
  const { user } = useAuth();
  return (
    <Formik
      initialValues={initialValues(initialState)}
      validationSchema={validationSchema()}
      onSubmit={async (values) => {
        const { error, message } = !isEdit
          ? await createAddress({ ...values, user: user.id })
          : await updateAddress({ values, id: initialState.id });

        if (error) {
          toast.error(message);
        } else {
          toast.success(message);
          setReloadData();
          closeModal();
        }
      }}
    >
      {({ isSubmitting }) => (
        <Form className='ui form'>
          <Input
            type='text'
            name='title'
            placeholder='Titulo de la dirección'
            label='Titulo de la dirección'
          />
          <FormGroup widths='equal'>
            <Input
              type='text'
              name='name'
              placeholder='Nombre y apellidos'
              label='Nombre y apellidos'
            />
            <Input type='text' name='address' placeholder='Dirección' label='Dirección' />
          </FormGroup>

          <FormGroup widths='equal'>
            <Input type='text' name='city' placeholder='Ciudad' label='Ciudad' />
            <Input
              type='text'
              name='state'
              placeholder='Estado/Provincia/Región'
              label='Estado/Provincia/Región'
            />
          </FormGroup>

          <FormGroup widths='equal'>
            <Input
              type='text'
              name='postalCode'
              placeholder='Código postal'
              label='Código postal'
            />
            <Input type='phone' name='phone' placeholder='Teléfono' label='Teléfono' />
          </FormGroup>

          <div className='actions'>
            <Button type='submit' className='submit' loading={isSubmitting} disabled={isSubmitting}>
              {isEdit ? 'Editar dirección' : 'Agregar dirección'}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

function initialValues(initialState) {
  return {
    title: initialState?.title || '',
    name: initialState?.name || '',
    address: initialState?.address || '',
    city: initialState?.city || '',
    state: initialState?.state || '',
    postalCode: initialState?.postalCode || '',
    phone: initialState?.phone || '',
  };
}

function validationSchema() {
  return Yup.object({
    title: Yup.string().required(),
    name: Yup.string().required(),
    address: Yup.string().required(),
    city: Yup.string().required(),
    state: Yup.string().required(),
    postalCode: Yup.string().length(5).required(),
    phone: Yup.string().length(10).required(),
  });
}
