import { Grid, Button } from 'semantic-ui-react';
import map from 'lodash/map';
import size from 'lodash/size';
import classNames from 'classnames';
import Link from 'next/link';
import { useData } from '@hooks/useData.hook';
import { getAddressesByUser } from '@services/api/address';
import { useCart } from '@hooks/useCart.hook';

export default function AddressShipping({ userId }) {
  const { data } = useData({ func: getAddressesByUser, params: { userId } });
  const isAddresses = size(data) > 0;

  return (
    <div className='address-shipping'>
      <div className='title'>Dirección de envió</div>
      <div className='data'>
        {!isAddresses && (
          <h3>
            No tienes direcciones registradas <Link href='/account'>Añada una dirección</Link>
          </h3>
        )}
        {isAddresses && (
          <Grid>
            {map(data, (address) => (
              <Grid.Column key={address.id} mobile={16} tablet={8} computer={4}>
                <Address {...address} />
              </Grid.Column>
            ))}
          </Grid>
        )}
      </div>
    </div>
  );
}

function Address(props) {
  const { id, title, name, address, city, state, postalCode, phone } = props;
  const { address: addressCart, addAddress } = useCart();

  return (
    <div
      className={classNames('address', { active: addressCart?.id === id })}
      onClick={() => addAddress({ id, title, name, address, city, state, postalCode, phone })}
    >
      <p>{title}</p>
      <p>{name}</p>
      <p>{address}</p>
      <p>
        {city}, {state} {postalCode}
      </p>
      <p>{phone}</p>
    </div>
  );
}
