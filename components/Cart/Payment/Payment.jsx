import React, { useState } from 'react';
import { Elements, CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { STRIPE_TOKEN } from 'constants/stripe.constant';
import { Button } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { useAuth } from '@hooks/useAuth';
import { paymentCart } from '@services/api/cart.api';
import { useCart } from '@hooks/useCart.hook';

const stripePromise = loadStripe(STRIPE_TOKEN);

export default function Payment({ products, address }) {
  return (
    <div className='payment'>
      <div className='title'>Pago</div>
      <div className='data'>
        <Elements stripe={stripePromise}>
          <FormPayment products={products} address={address} />
        </Elements>
      </div>
    </div>
  );
}

function FormPayment({ address, products }) {
  const { user } = useAuth();
  const [loading, setLoading] = useState(false);
  const { clearCart } = useCart();

  const stripe = useStripe();
  const elements = useElements();

  const isDisable = !stripe || loading;

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (!stripe || !elements) {
      toast.error('Error al realizar el pago');
      setLoading(false);
      return;
    }

    const cardElement = elements.getElement(CardElement);
    const result = await stripe.createToken(cardElement);

    if (result.error) {
      toast.error(result.error.message);
      setLoading(false);
      return;
    }
    const resp = await paymentCart({
      token: result.token,
      idUser: user._id,
      address,
      products,
    });

    if (resp.error) {
      toast.error(resp.message);
      setLoading(false);
      return;
    }

    toast.success(resp.message);

    clearCart();

    setLoading(false);
  };

  return (
    <form className='form-payment' onSubmit={handleSubmit}>
      <CardElement />
      <Button type='submit' loading={loading} disabled={isDisable}>
        Pagar
      </Button>
    </form>
  );
}
