import { Container } from 'semantic-ui-react';
import Head from 'next/head';
import { Header } from '@components/Header';
import classNames from 'classnames';
const siteTitle = 'Gaming';

export const Layout = ({ children, title, description, className = '' }) => {
  return (
    <>
      <Head>
        <title>{!title ? siteTitle : `${title} | ${siteTitle}`}</title>
        <meta property='description' content={description} />
      </Head>
      <Container fluid className={classNames('basic-layout', { [className]: className })}>
        <Header />
        <Container className='content'>{children}</Container>
      </Container>
    </>
  );
};

Layout.defaultProps = {
  title: 'Gaming | Tus juegos favoritos',
  description: 'Tus juegos favoritos para Steam, PlayStation, Xbox, Switch al mejor precio.',
};
