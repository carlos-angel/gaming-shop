import { Modal, Icon } from 'semantic-ui-react';

export const BasicModal = (props) => {
  const { children, show, setShow, title, ...rest } = props;

  const onClose = () => setShow();

  return (
    <Modal className='basic-modal' open={show} onClose={onClose} {...rest}>
      <Modal.Header>
        <span>{title}</span> <Icon name='close' onClick={onClose} />
      </Modal.Header>
      <Modal.Content>{children}</Modal.Content>
    </Modal>
  );
};
