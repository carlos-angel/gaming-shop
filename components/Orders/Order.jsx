import { Image, Icon } from 'semantic-ui-react';
import Link from 'next/link';
import moment from 'moment';
import { BasicModal } from '@components/Modal/BasicModal';
import { useModal } from '@hooks/useModal';

import 'moment/locale/es';

export default function Order({ game, totalPayment, createdAt, addressShipping }) {
  const { title, poster, url } = game;
  const { isOpen, title: titleModal, content, openModal, closeModal } = useModal();

  return (
    <>
      <div className='order'>
        <div className='order__info'>
          <div className='order__info-data'>
            <Link href={`/${url}`}>
              <a>
                <Image src={poster.url} alt={title} />
              </a>
            </Link>
            <div>
              <h2>{title}</h2>
              <p>$ {totalPayment}.00</p>
            </div>
          </div>
          <div className='order__other'>
            <p className='order__other-date'>
              {moment(createdAt).format('L')} - {moment(createdAt).format('LT')}
            </p>
            <Icon
              name='eye'
              circular
              link
              onClick={() =>
                openModal({
                  content: <AddressShipping {...addressShipping} />,
                  title,
                })
              }
            />
          </div>
        </div>
      </div>
      <BasicModal show={isOpen} setShow={closeModal} title={titleModal}>
        {content}
      </BasicModal>
    </>
  );
}

function AddressShipping({ title, name, address, state, city, postalCode, phone }) {
  return (
    <>
      <h3>El pedido se ha enviado a la siguiente dirección:</h3>
      <div className='address'>
        <p>{title}</p>
        <p>{name}</p>
        <p>{address}</p>
        <p>{`${state}, ${city} ${postalCode}`}</p>
        <p>{phone}</p>
      </div>
    </>
  );
}
