import { Grid } from 'semantic-ui-react';
import { map, size } from 'lodash';
import Order from './Order';
export default function Orders({ orders }) {
  const isHasOrders = size(orders) !== 0;
  return !isHasOrders ? (
    <h2 style={{ textAlign: 'center' }}>Todavía no has realizado ninguna compra</h2>
  ) : (
    <Grid>
      {map(orders, (order) => (
        <Grid.Column mobile={16} tablet={6} computer={8} key={order._id}>
          <Order {...order} />
        </Grid.Column>
      ))}
    </Grid>
  );
}
