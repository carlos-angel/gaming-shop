import React from 'react';
import { Image, Grid } from 'semantic-ui-react';
import Link from 'next/link';
import useWindowSize from '@hooks/useWindowSize.hook';
import { getColumnsResponsive } from '@utils/breakpoints.util';

export default function Games({ children }) {
  const { width } = useWindowSize();
  const columns = getColumnsResponsive(width);

  return (
    <div className='list-games'>
      <Grid>
        <Grid.Row columns={columns}>{children}</Grid.Row>
      </Grid>
    </div>
  );
}

function Game({ url, poster, title, discount, price }) {
  return (
    <Grid.Column className='list-games__game'>
      <Link href={`/${url}`}>
        <a>
          <div className='list-games__game-poster'>
            <Image src={poster.url} alt={title} />
            <div className='list-games__game-poster-info'>
              {discount ? <span className='discount'>{`${discount}%`}</span> : <span />}
              <span className='price'>${price}</span>
            </div>
          </div>
          <h2>{title}</h2>
        </a>
      </Link>
    </Grid.Column>
  );
}

Games.Item = Game;
