import { useField } from 'formik';
import { FormInput } from 'semantic-ui-react';

export const Input = ({ label, ...props }) => {
  const [field, meta] = useField(props);

  return <FormInput label={label} {...field} {...props} error={meta.error && meta.touched} />;
};
