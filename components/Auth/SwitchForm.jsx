import { useState, useContext } from 'react';
import { MenuContext } from '@components/Header/Menu';
import { LoginForm } from '@components/Auth/LoginForm';
import { RegisterForm } from '@components/Auth/RegisterForm';
import { ForgotPasswordForm } from '@components/Auth/ForgotPassword';

export const SwitchForm = () => {
  const { setModalContent } = useContext(MenuContext);

  const changeLogin = () =>
    setModalContent({
      title: 'Iniciar sesión',
      content: (
        <LoginForm changeRegister={changeRegister} changeForgotPassword={changeForgotPassword} />
      ),
    });

  const changeRegister = () =>
    setModalContent({ title: 'Registrarse', content: <RegisterForm changeLogin={changeLogin} /> });

  const changeForgotPassword = () =>
    setModalContent({
      title: 'Recuperar contraseña',
      content: <ForgotPasswordForm changeLogin={changeLogin} />,
    });

  return <LoginForm changeRegister={changeRegister} changeForgotPassword={changeForgotPassword} />;
};
