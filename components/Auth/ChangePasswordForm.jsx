import { Button, FormGroup } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Input } from '@components/Form';
import { useAuth } from '@hooks/useAuth';
import { updateProfile } from '@services/api/profile.api';

function initialValues() {
  return {
    password: '',
    confirmPassword: '',
  };
}

function validationSchema() {
  return Yup.object({
    password: Yup.string().required(),
    confirmPassword: Yup.string()
      .required()
      .oneOf([Yup.ref('password')]),
  });
}

export const ChangePasswordForm = () => {
  const { user } = useAuth();

  return (
    <div className='change-password-form'>
      <h4>Actualizar contraseña</h4>
      <Formik
        initialValues={initialValues()}
        validationSchema={validationSchema()}
        onSubmit={async ({ password }) => {
          const { error, message } = await updateProfile({ id: user.id, body: { password } });
          error ? toast.warning(message) : toast.success(message);
        }}
      >
        {({ isSubmitting }) => (
          <Form className='ui form'>
            <FormGroup widths='equal'>
              <Input type='password' name='password' placeholder='nueva contraseña' />
              <Input
                type='password'
                name='confirmPassword'
                placeholder='confirmar nueva contraseña'
              />
            </FormGroup>

            <Button type='submit' loading={isSubmitting} className='submit' disabled={isSubmitting}>
              Actualizar
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};
