import { useContext } from 'react';
import { Button } from 'semantic-ui-react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { Input } from '@components/Form';
import { MenuContext } from '@components/Header/Menu';
import { login } from '@services/api/auth.api';
import { useAuth } from '@hooks/useAuth';

export const LoginForm = ({ changeRegister, changeForgotPassword }) => {
  const { signIn } = useAuth();
  const { closeModal } = useContext(MenuContext);
  return (
    <Formik
      initialValues={initialValues()}
      validationSchema={validationSchema()}
      onSubmit={async (values) => {
        const { error, message, data } = await login(values);
        data?.jwt && signIn(data.jwt);
        error && toast.warning(message);
        !error && closeModal();
      }}
    >
      {({ isSubmitting }) => (
        <Form className='ui form auth-form'>
          <Input type='email' name='identifier' placeholder='Correo electrónico' />
          <Input type='password' name='password' placeholder='Contraseña' />

          <div className='actions'>
            <Button type='button' basic onClick={changeRegister} disabled={isSubmitting}>
              Nuevo usuario
            </Button>
            <div>
              <Button
                type='submit'
                className='submit'
                loading={isSubmitting}
                disabled={isSubmitting}
              >
                Iniciar sesión
              </Button>
              <Button type='button' onClick={changeForgotPassword} disabled={isSubmitting}>
                ¿Has olvidado tu contraseña?
              </Button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

function initialValues() {
  return {
    identifier: '',
    password: '',
  };
}

function validationSchema() {
  return Yup.object({
    identifier: Yup.string().email().required(),
    password: Yup.string().required(),
  });
}
