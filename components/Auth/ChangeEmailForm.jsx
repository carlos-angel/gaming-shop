import { Button, FormGroup } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Input } from '@components/Form';
import { useAuth } from '@hooks/useAuth';
import { updateProfile } from '@services/api/profile.api';

function initialValues() {
  return {
    email: '',
    confirmEmail: '',
  };
}

function validationSchema() {
  return Yup.object({
    email: Yup.string().email().required(),
    confirmEmail: Yup.string()
      .email()
      .required()
      .oneOf([Yup.ref('email')]),
  });
}

export const ChangeEmailForm = ({ currentEmail }) => {
  const { user, setReloadUser } = useAuth();

  return (
    <div className='change-name-form'>
      <h4>
        Cambia tu e-mail
        <span>{` (tu e-mail actual: ${currentEmail})`}</span>
      </h4>
      <Formik
        initialValues={initialValues()}
        validationSchema={validationSchema()}
        onSubmit={async (values) => {
          const { error, message } = await updateProfile({ id: user.id, body: values });
          !error && setReloadUser(true);
          error ? toast.warning(message) : toast.success(message);
        }}
      >
        {({ isSubmitting }) => (
          <Form className='ui form'>
            <FormGroup widths='equal'>
              <Input type='email' name='email' placeholder='Nuevo e-mail' />
              <Input type='email' name='confirmEmail' placeholder='Confirma e-mail' />
            </FormGroup>

            <Button type='submit' loading={isSubmitting} className='submit' disabled={isSubmitting}>
              Actualizar
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};
