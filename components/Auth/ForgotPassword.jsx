import { Button } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Input } from '@components/Form';
import { forgotPassword } from '@services/api/auth.api';

export const ForgotPasswordForm = ({ changeLogin }) => {
  return (
    <Formik
      initialValues={initialValues()}
      validationSchema={validationSchema()}
      onSubmit={async ({ email }) => {
        const { error, message } = await forgotPassword({ email });
        error ? toast.warning(message) : toast.success(message);
        !error && changeLogin();
      }}
    >
      {({ isSubmitting }) => (
        <Form className='ui form login-form'>
          <Input type='email' name='email' placeholder='Correo electrónico' />

          <div className='actions'>
            <Button type='button' basic onClick={changeLogin} disabled={isSubmitting}>
              Iniciar sesión
            </Button>
            <Button type='submit' className='submit' loading={isSubmitting} disabled={isSubmitting}>
              Recuperar Contraseña
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

function initialValues() {
  return {
    email: '',
  };
}

function validationSchema() {
  return Yup.object({
    email: Yup.string().email().required(),
  });
}
