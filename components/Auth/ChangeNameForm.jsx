import { Button, FormGroup } from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Input } from '@components/Form';
import { useAuth } from '@hooks/useAuth';
import { updateProfile } from '@services/api/profile.api';

function initialValues({ name, lastName }) {
  return {
    name,
    lastName,
  };
}

function validationSchema() {
  return Yup.object({
    name: Yup.string().required(),
    lastName: Yup.string().required(),
  });
}

export const ChangeNameForm = ({ name, lastName }) => {
  const { user } = useAuth();

  return (
    <div className='change-name-form'>
      <h4>Cambia tu nombre y apellidos</h4>
      <Formik
        initialValues={initialValues({ name, lastName })}
        validationSchema={validationSchema()}
        onSubmit={async (values) => {
          const { error, message } = await updateProfile({ id: user.id, body: values });
          error ? toast.warning(message) : toast.success(message);
        }}
      >
        {({ isSubmitting }) => (
          <Form className='ui form'>
            <FormGroup widths='equal'>
              <Input type='text' name='name' placeholder='Nombre' />
              <Input type='text' name='lastName' placeholder='Apellidos' />
            </FormGroup>

            <Button type='submit' loading={isSubmitting} className='submit' disabled={isSubmitting}>
              Actualizar
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};
