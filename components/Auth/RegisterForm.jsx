import { Button } from 'semantic-ui-react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Input } from '@components/Form';
import { register } from '@services/api/auth.api';
import { toast } from 'react-toastify';

export const RegisterForm = ({ changeLogin }) => {
  return (
    <Formik
      initialValues={initialValues()}
      validationSchema={validationSchema()}
      onSubmit={async (values) => {
        const { error, message } = await register(values);
        error ? toast.error(message) : toast.success(message);
        !error && changeLogin();
      }}
    >
      {({ isSubmitting }) => (
        <Form className='ui form auth-form'>
          <Input type='text' name='name' placeholder='Nombre' />
          <Input type='text' name='lastName' placeholder='Apellidos' />
          <Input type='text' name='username' placeholder='Nombre de usuario' />
          <Input type='email' name='email' placeholder='Correo electrónico' />
          <Input type='password' name='password' placeholder='Contraseña' />

          <div className='actions'>
            <Button type='button' basic onClick={changeLogin} disabled={isSubmitting}>
              Iniciar sesión
            </Button>
            <Button type='submit' className='submit' loading={isSubmitting} disabled={isSubmitting}>
              Registrase
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

function initialValues() {
  return {
    name: '',
    lastName: '',
    username: '',
    email: '',
    password: '',
  };
}

function validationSchema() {
  return Yup.object({
    name: Yup.string().required(),
    lastName: Yup.string().required(),
    username: Yup.string().required(),
    email: Yup.string().required(),
    password: Yup.string().required(),
  });
}
