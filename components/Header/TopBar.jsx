import Link from 'next/link';
import { useRouter } from 'next/router';
import { Container, Image, Grid } from 'semantic-ui-react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Input } from '@components/Form';

export function TopBar() {
  return (
    <div className='top-bar'>
      <Container>
        <Grid className='top-bar'>
          <Grid.Column width={8} className='top-bar__left'>
            <Logo />
          </Grid.Column>

          <Grid.Column width={8} className='top-bar__right'>
            <Search />
          </Grid.Column>
        </Grid>
      </Container>
    </div>
  );
}

function Logo() {
  return (
    <Link href='/'>
      <a>
        <Image src='/logo.png' alt='Gaming' />
      </a>
    </Link>
  );
}

function Search() {
  const router = useRouter();

  return (
    <Formik
      initialValues={{ query: '' }}
      validationSchema={Yup.object({
        query: Yup.string().required('Este campo es requerido'),
      })}
      onSubmit={({ query }) => {
        router.push(`/search?q=${query}`);
      }}
    >
      {() => (
        <Form>
          <Input
            id='search-game'
            icon={{ name: 'search' }}
            type='string'
            name='query'
            placeholder='search'
          />
        </Form>
      )}
    </Formik>
  );
}
