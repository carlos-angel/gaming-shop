import { SwitchForm } from '@components/Auth/SwitchForm';
import { useContext } from 'react';
import { Menu, Icon } from 'semantic-ui-react';
import { MenuContext } from './Menu';

export default function Options() {
  const { openModal } = useContext(MenuContext);

  return (
    <Menu>
      <Menu.Item onClick={() => openModal({ title: 'Iniciar sesión', content: <SwitchForm /> })}>
        <Icon name='user outline' />
        Mi cuenta
      </Menu.Item>
    </Menu>
  );
}
