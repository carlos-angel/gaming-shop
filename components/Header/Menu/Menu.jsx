import React, { useState, createContext } from 'react';
import { Container, Grid } from 'semantic-ui-react';
import { BasicModal } from '@components/Modal/BasicModal';
import UserOptions from '@components/Header/Menu/UserOptions';
import Options from '@components/Header/Menu/Options';
import Platforms from '@components/Header/Menu/Platforms';
import { useAuth } from '@hooks/useAuth';
import { useModal } from '@hooks/useModal';

export const MenuContext = createContext({
  openModal: (show) => {},
  changeTitleModal: (title) => {},
});
const { Provider } = MenuContext;

export function Menu() {
  const { isOpen, title, content, openModal, closeModal, setModalContent } = useModal();
  const { isAuth, user } = useAuth();

  return (
    <Provider value={{ openModal, setModalContent, closeModal }}>
      <div className='menu'>
        <Container>
          <Grid>
            <Grid.Column className='menu__left' width={6}>
              <Platforms />
            </Grid.Column>
            <Grid.Column className='menu__right' width={10}>
              {isAuth && user?.username ? <UserOptions username={user.username} /> : <Options />}
            </Grid.Column>
          </Grid>
        </Container>
        <BasicModal show={isOpen} setShow={closeModal} title={title} size='small'>
          {content}
        </BasicModal>
      </div>
    </Provider>
  );
}
