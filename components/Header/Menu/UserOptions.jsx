import { Menu, Icon, Label } from 'semantic-ui-react';
import Link from 'next/link';
import { useAuth } from '@hooks/useAuth';
import { useCart } from '@hooks/useCart.hook';

export default function UserOptions({ username }) {
  const { signOut } = useAuth();
  const { quantity } = useCart();

  return (
    <Menu>
      <Link href='/orders' passHref>
        <Menu.Item>
          <Icon name='game' />
          Mis Pedidos
        </Menu.Item>
      </Link>
      <Link href='/wishlist' passHref>
        <Menu.Item>
          <Icon name='heart outline' />
          Mis Favoritos
        </Menu.Item>
      </Link>
      <Link href='/account' passHref>
        <Menu.Item>
          <Icon name='user outline' />
          {username}
        </Menu.Item>
      </Link>
      <Link href='/cart' passHref>
        <Menu.Item className='m-0'>
          <Icon name='cart' />
          {quantity > 0 && (
            <Label color='red' floating circular>
              {quantity}
            </Label>
          )}
        </Menu.Item>
      </Link>
      <Menu.Item className='m-0' onClick={signOut}>
        <Icon name='power off' />
      </Menu.Item>
    </Menu>
  );
}
