import { useData } from '@hooks/useData.hook';
import { getPlatforms } from '@services/api/platforms.api';
import map from 'lodash/map';
import Link from 'next/link';
import { Menu } from 'semantic-ui-react';

export default function Platforms() {
  const { data } = useData({ func: getPlatforms, params: null });

  if (!data) return null;

  return (
    <Menu>
      {map(data, (platform) => (
        <Link href={`/games/${platform.url}`} passHref key={platform.id}>
          <Menu.Item link>{platform.title}</Menu.Item>
        </Link>
      ))}
    </Menu>
  );
}
