import { TopBar } from './TopBar';
import { Menu } from './Menu';

export function Header() {
  return (
    <div className='header'>
      <TopBar />
      <Menu />
    </div>
  );
}
