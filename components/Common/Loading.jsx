import React from 'react';
import { Dimmer, Loader, Segment } from 'semantic-ui-react';

export default function Loading({ content }) {
  return (
    <Segment basic style={{ marginBottom: '20px' }}>
      <Dimmer active inverted>
        <Loader size='small'>{content}</Loader>
      </Dimmer>
    </Segment>
  );
}
