import React from 'react';
import { Pagination as PaginationSUR } from 'semantic-ui-react';
import { useRouter } from 'next/router';
import queryString from 'query-string';

export default function Pagination({ total, page, limit }) {
  const totalPages = Math.ceil(total / limit);
  const router = useRouter();
  const onPageChange = (e, { activePage }) => {
    const url = queryString.parseUrl(router.asPath);
    url.query.page = activePage;
    router.push(queryString.stringifyUrl(url));
  };

  return (
    <div className='pagination'>
      <PaginationSUR
        defaultActivePage={page}
        totalPages={totalPages}
        firstItem={null}
        lastItem={null}
        onPageChange={onPageChange}
        boundaryRange={0}
        siblingRange={1}
        ellipsisItem={null}
      />
    </div>
  );
}
