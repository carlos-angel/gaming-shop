import React from 'react';
import { Tab } from 'semantic-ui-react';

export function PaneInformation({ children }) {
  return (
    <Tab.Pane>
      <div className='pane-information'>{children}</div>
    </Tab.Pane>
  );
}
