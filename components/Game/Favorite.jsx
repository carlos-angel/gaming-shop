import { useState } from 'react';
import { Icon } from 'semantic-ui-react';
import { useData } from '@hooks/useData.hook';
import { addFavoriteGame } from '@services/api/favorite.api';
import { deleteFavoriteGame } from '@services/api/favorite.api';
import { isFavoriteGame } from '@services/api/favorite.api';
import classNames from 'classnames';

export default function Favorite({ userId, gameId }) {
  const [loading, setLoading] = useState(false);

  const { data, setReloadData } = useData({
    func: isFavoriteGame,
    params: { userId, gameId },
    paramsEffect: [gameId],
  });
  const isFavorite = data && data[0] ? true : false;

  const handleFavorite = async () => {
    if (userId) {
      setLoading(true);
      if (!isFavorite) {
        await addFavoriteGame({ userId, gameId });
      } else {
        await deleteFavoriteGame({ id: data[0].id });
      }
      setLoading(false);
      setReloadData();
    }
  };
  return (
    <Icon
      name={isFavorite ? 'heart' : 'heart outline'}
      className={classNames({ like: isFavorite })}
      loading={loading}
      disabled={loading}
      onClick={handleFavorite}
      link
    />
  );
}
