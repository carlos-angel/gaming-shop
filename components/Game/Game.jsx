import { Grid, Image } from 'semantic-ui-react';
import { useAuth } from '@hooks/useAuth';
import { priceGameWithDiscount } from '@utils/price-game-with-discount.util';
import Favorite from './Favorite';

export default function Game({ children }) {
  return <Grid className='header-game'>{children}</Grid>;
}

function Poster({ src, alt }) {
  return (
    <Grid.Column mobile={16} tablet={6} computer={5}>
      <Image src={src} alt={alt} fluid />
    </Grid.Column>
  );
}

function Information({ children }) {
  return (
    <Grid.Column mobile={16} tablet={10} computer={11}>
      {children}
    </Grid.Column>
  );
}

function Title({ title, gameId }) {
  const { user, isAuth } = useAuth();

  return (
    <div className='header-game__title'>
      {title}
      {isAuth && <Favorite userId={user.id} gameId={gameId} />}
    </div>
  );
}

function Delivery({ text }) {
  return (
    <div className='header-game__delivery'>
      <p>{text}</p>
    </div>
  );
}

function Summary({ text }) {
  return <div className='header-game__summary' dangerouslySetInnerHTML={{ __html: text }} />;
}
function Buy({ price, discount, children }) {
  return (
    <div className='header-game__buy'>
      <div className='header-game__buy-price'>
        <p>{`Precio de venta al publico: $${price}`}</p>
        <div className='header-game__buy-price-actions'>
          <p>{`-${discount}%`}</p>
          {<p>{`$${priceGameWithDiscount(price, discount)}`}</p>}
        </div>
      </div>
      {children}
    </div>
  );
}

Game.Poster = Poster;
Game.Information = Information;

Game.Title = Title;
Game.Delivery = Delivery;
Game.Summary = Summary;
Game.Buy = Buy;
