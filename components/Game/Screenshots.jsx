import React from 'react';
import { Image, Modal } from 'semantic-ui-react';
import Slider from 'react-slick';
import map from 'lodash/map';
import { useModal } from '@hooks/useModal';

const settings = {
  className: 'carousel-screenshots',
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 3,
  swipeToSlide: true,
};

export default function Screenshots({ screenshots, title }) {
  const { isOpen, content, openModal, closeModal } = useModal();

  return (
    <>
      <Slider {...settings}>
        {map(screenshots, (screenshot) => (
          <Image
            key={screenshot.id}
            src={screenshot.url}
            alt={screenshot.name}
            onClick={() => openModal({ content: <Image src={screenshot.url} alt={title} /> })}
          />
        ))}
      </Slider>
      <Modal open={isOpen} onClose={closeModal} size='large'>
        {content}
      </Modal>
    </>
  );
}
