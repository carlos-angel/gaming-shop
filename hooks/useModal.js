import React, { useReducer } from 'react';

const initialState = {
  isOpen: false,
  content: null,
  title: null,
};

const modalTypes = {
  open: 'open',
  close: 'close',
  content: 'content',
};

const modalReducer = (state, action) => {
  switch (action.type) {
    case modalTypes.open:
      return {
        ...state,
        isOpen: true,
        content: action.payload.content,
        title: action.payload.title,
      };
    case modalTypes.close:
      return {
        ...state,
        isOpen: false,
        content: null,
        title: null,
      };
    case modalTypes.content:
      return {
        ...state,
        content: action.payload.content,
        title: action.payload.title,
      };
    default:
      return state;
  }
};

const modalActions = {
  open: ({ content, title }) => ({ type: modalTypes.open, payload: { content, title } }),
  close: () => ({ type: modalTypes.close }),
  content: ({ content, title }) => ({ type: modalTypes.content, payload: { content, title } }),
};

export function useModal() {
  const [state, dispatch] = useReducer(modalReducer, initialState);
  const openModal = ({ content, title }) => dispatch(modalActions.open({ content, title }));
  const closeModal = () => dispatch(modalActions.close());
  const setModalContent = ({ content, title }) =>
    dispatch(modalActions.content({ content, title }));

  return { ...state, openModal, closeModal, setModalContent };
}
