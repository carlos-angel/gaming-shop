/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useReducer } from 'react';
import { dataReducer, INITIAL_STATE, actions } from 'reducers/data.reducer';
import { toast } from 'react-toastify';

export const useData = ({ func, params, paramsEffect = [] }) => {
  const [state, dispatch] = useReducer(dataReducer, INITIAL_STATE);
  const controller = new AbortController();
  const config = { signal: controller.signal };

  useEffect(() => {
    fetchData();
    return () => controller.abort();
  }, paramsEffect);

  const fetchData = () => {
    dispatch(actions.loading());
    func({ ...params, config })
      .then(({ error, data, message }) => {
        if (error) {
          const isAbort = message === 'canceled';
          !isAbort && toast.error(message);
          !isAbort && dispatch(actions.error());
        } else {
          dispatch(actions.success(data));
        }
      })
      .catch((err) => toast.error(err.message));
  };

  const setReloadData = () => fetchData();

  return { ...state, setReloadData };
};
